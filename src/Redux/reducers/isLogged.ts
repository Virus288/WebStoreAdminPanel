interface action {
  type: string;
}

interface stateInterface {
  isLogged: boolean;
}

export const loggedReducer = (state: stateInterface = { isLogged: false }, action: action): stateInterface => {
  switch (action.type) {
    case 'SING_IN':
      return (state = { isLogged: true });
    case 'SING_OUT':
      return (state = { isLogged: false });
    default:
      return state;
  }
};
