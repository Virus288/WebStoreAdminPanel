interface action {
  type: string;
  payload: {
    message: string;
  };
}

interface stateInterface {
  message: string;
}

export const headerReducer = (state: stateInterface = { message: '' }, action: action): stateInterface => {
  switch (action.type) {
    case 'TOGGLE_HEADER':
      return (state = { message: action.payload.message });
    default:
      return state;
  }
};
