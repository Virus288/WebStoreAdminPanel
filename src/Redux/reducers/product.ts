import { ProductsInterface } from '../../Components/Products/Interfaces/productsInterface';

interface localProductInterface {
  payload: {
    selectedProduct: string;
    products: ProductsInterface[];
  };
  type: string;
}

interface stateInterface {
  products: ProductsInterface[];
}

export const productReducer = (
  state: stateInterface = { products: [] },
  action: localProductInterface,
): stateInterface => {
  switch (action.type) {
    case 'ADD_PRODUCTS':
      return (state = { products: action.payload.products });
    case 'REMOVE_PRODUCTS':
      if (state.products) {
        return (state = {
          products: state.products.filter((product) => {
            return product._id != action.payload.selectedProduct;
          }),
        });
      } else {
        return state;
      }
    case 'REMOVE_ALL_PRODUCTS':
      return (state = { products: [] });
    default:
      return state;
  }
};
