import { combineReducers } from 'redux';
import { loggedReducer } from './isLogged';
import { popupReducer } from './popup';
import { productReducer } from './product';
import { headerReducer } from './header';

const rootReducer = combineReducers({
  isLogged: loggedReducer,
  popup: popupReducer,
  products: productReducer,
  header: headerReducer,
});

export default rootReducer;
