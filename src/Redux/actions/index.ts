import { ProductsInterface } from '../../Components/Products/Interfaces/productsInterface';

export const logIn = (): { type: string } => {
  return {
    type: 'SING_IN',
  };
};

export const logOut = (): { type: string } => {
  return {
    type: 'SING_OUT',
  };
};

export const enablePopup = (text: string): { type: string; payload: { text: string } } => {
  return {
    type: 'ENABLE_POPUP',
    payload: { text },
  };
};

export const disablePopup = (): { type: string } => {
  return {
    type: 'DISABLE_POPUP',
  };
};

export const addProducts = (
  products: ProductsInterface[],
): { type: string; payload: { products: ProductsInterface[] } } => {
  return {
    type: 'ADD_PRODUCTS',
    payload: { products },
  };
};

export const getProducts = (): { type: string } => {
  return {
    type: 'GET_PRODUCTS',
  };
};

export const getProduct = (): { type: string } => {
  return {
    type: 'GET_PRODUCT',
  };
};

export const getCategory = (category: string): { type: string; payload: { category: string } } => {
  return {
    type: 'GET_CATEGORY',
    payload: { category },
  };
};

export const removeProducts = (): { type: string } => {
  return {
    type: 'REMOVE_PRODUCTS',
  };
};

export const toggleHeader = (
  message: string,
): {
  type: string;
  payload: {
    message: string;
  };
} => {
  return {
    type: 'TOGGLE_HEADER',
    payload: { message },
  };
};
