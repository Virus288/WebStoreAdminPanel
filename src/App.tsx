import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import './Css/style.css';
import './Css/fontello/css/fontello.css';
import { Routers } from './Routers/Router';
import { disablePopup, enablePopup, logIn } from './Redux/actions';
import { Navbar } from './Components/Generic/Navbar';
import { Popup } from './Components/Generic/Popup';
import { checkIfLogged } from './Components/Account/Handlers/loginHandlers';

export const App: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    checkIfLogged().then((data): void => {
      if (!data.success) {
        dispatch(enablePopup(data.message));
        setTimeout(() => {
          dispatch(disablePopup());
        }, 3500);
      } else {
        dispatch(logIn());
      }
    });
  }, []);

  return (
    <Router>
      <div className="App">
        <Navbar />
        <Popup />
        <div className="content">
          <Routers />
        </div>
      </div>
    </Router>
  );
};
