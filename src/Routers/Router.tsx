import React from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch, useLocation } from 'react-router-dom';
import { RootState } from '../index';
import { HomePanel } from '../Components/Home/Home';
import { Products } from '../Components/Products/views/Products';
import { Orders } from '../Components/Orders/Views/Orders';
import { Users } from '../Components/Users/Views/Users';
import { Messages } from '../Components/Messages/Views/Messages';
import { FourOhFour } from '../Components/FourOhFour/FourOhFour';

export const Routers: React.FC = () => {
  const location = useLocation();
  const isLogged = useSelector((state: RootState) => state.isLogged.isLogged);

  return (
    <Switch location={location} key={location.key}>
      <Route exact path="/" render={(): JSX.Element => <HomePanel />} />
      <Route exact path="/produkty" render={(): JSX.Element => (isLogged ? <Products /> : <HomePanel />)} />
      <Route exact path="/uzytkownicy" render={(): JSX.Element => (isLogged ? <Users /> : <HomePanel />)} />
      <Route exact path="/zamowienia" render={(): JSX.Element => (isLogged ? <Orders /> : <HomePanel />)} />
      <Route exact path="/wiadomosci" render={(): JSX.Element => (isLogged ? <Messages /> : <HomePanel />)} />
      <Route path="*" render={(): JSX.Element => <FourOhFour />} />
    </Switch>
  );
};
