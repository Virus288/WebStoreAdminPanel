import { LoginSuccessInterface } from '../Components/Products/Interfaces/productsInterface';

export interface ErrorCodes {
  errCode: number;
  error: string;
}

export enum ErrorResponsesPL {
  SERVER_DOWN = 'Błąd serwera',
  AUTH_FAILED = 'Nie zalogowano',
  ENDPOINT_NOT_FOUND = 'Podany element nie istnieje',
  WRONG_TYPE = 'Sesja wygasła',
}

export const errorCodesHandler = (error: ErrorCodes): LoginSuccessInterface => {
  switch (error.errCode) {
    case 400:
      return { message: ErrorResponsesPL.WRONG_TYPE, success: false };
    case 401:
      return { message: ErrorResponsesPL.AUTH_FAILED, success: false };
    case 404:
      return { message: ErrorResponsesPL.ENDPOINT_NOT_FOUND, success: false };
    case 500:
      return { message: ErrorResponsesPL.SERVER_DOWN, success: false };
    default:
      console.log({ message: 'Error: Unknown error', error: error });
      return { message: error.error, success: false };
  }
};
