export interface ProductsInterface {
  _id: string;
  name: string;
  price: number;
  shortDescription: string;
  fullDescription: string;
  size: string;
  type: string;
  color: string;
  amount: number;
  ready: boolean;
  image?: ImageInterface;
}

export interface ProductsInterfaceWithoutImage {
  _id: string;
  name: string;
  price: number;
  shortDescription: string;
  fullDescription: string;
  size: string;
  type: string;
  color: string;
  amount: number;
  ready: boolean;
}

export interface NewProductsInterface {
  name: string;
  price: number;
  shortDescription: string;
  fullDescription: string;
  size: string;
  type: string;
  color: string;
  amount: number;
  ready: boolean;
  image?: ImageInterface;
}

export interface ImageInterface {
  icon: string;
  mainImage: string;
  additionalImages: string;
}

export interface EditProductInterface {
  screenName: string;
  product: string;
}

export interface LoginSuccessInterface {
  success: boolean;
  message: string;
}

export interface EditProductInterfaceNew {
  product: ProductsInterface;
  disableEdit: () => void;
}

export interface AddNewProductInterface {
  setProducts: React.Dispatch<React.SetStateAction<ProductsInterface[]>>;
  setFiltered: React.Dispatch<React.SetStateAction<ProductsInterface[]>>;
  disableAdd: () => void;
}
