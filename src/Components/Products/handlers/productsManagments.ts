import { errorCodesHandler } from '../../../Errors/errorCodesHandler';
import { LoginSuccessInterface, NewProductsInterface, ProductsInterface } from '../Interfaces/productsInterface';

export const getAllProducts = async (): Promise<ProductsInterface[] | LoginSuccessInterface> => {
  try {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/products`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
    });
    const data = await res.json();
    if (!res.ok) {
      return errorCodesHandler({ errCode: res.status, error: data.error });
    } else {
      return data;
    }
  } catch (err) {
    console.log(err);
    return { message: 'failed', success: false };
  }
};

export const editProductWithImages = async (
  category: string,
  id: string,
  formData: FormData,
): Promise<LoginSuccessInterface> => {
  try {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/products/${category}/${id}`, {
      method: 'PATCH',
      credentials: 'include',
      body: formData,
    });
    const data = await res.json();
    if (!res.ok) {
      return errorCodesHandler({ errCode: res.status, error: data.error });
    } else {
      return data;
    }
  } catch (err) {
    console.log(err);
    return { message: 'failed', success: false };
  }
};

export const removeImage = async (id: string, imageType: string): Promise<LoginSuccessInterface> => {
  try {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/image/remove/${id}/${imageType}`, {
      method: 'GET',
      credentials: 'include',
    });
    const data = await res.json();
    if (!res.ok) {
      return errorCodesHandler({ errCode: res.status, error: data.error });
    } else {
      return data;
    }
  } catch (err) {
    console.log(err);
    return { message: 'failed', success: false };
  }
};

export const postProduct = async (
  product: NewProductsInterface,
): Promise<ProductsInterface | LoginSuccessInterface> => {
  try {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/products`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
      body: JSON.stringify(product),
    });
    const data = await res.json();
    if (!res.ok) {
      return errorCodesHandler({ errCode: res.status, error: data.error });
    } else {
      return data;
    }
  } catch (err) {
    console.log(err);
    return { message: 'failed', success: false };
  }
};

export const postProductWithImages = async (formData: FormData): Promise<LoginSuccessInterface> => {
  try {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/products`, {
      method: 'PUT',
      credentials: 'include',
      body: formData,
    });
    const data = await res.json();
    if (!res.ok) {
      return errorCodesHandler({ errCode: res.status, error: data.error });
    } else {
      return data;
    }
  } catch (err) {
    console.log(err);
    return { message: 'failed', success: false };
  }
};

export const removeProduct = async (category: string, productId: string): Promise<LoginSuccessInterface> => {
  try {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/products/${category}/${productId}`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
    });
    const data = await res.json();
    if (!res.ok) {
      return errorCodesHandler({ errCode: res.status, error: data.error });
    } else {
      return data;
    }
  } catch (err) {
    console.log(err);
    return { message: 'failed', success: false };
  }
};
