import { postProductWithImages } from './productsManagments';
import { LoginSuccessInterface } from '../Interfaces/productsInterface';

export const handleSubmit = async (form: HTMLFormElement): Promise<LoginSuccessInterface> => {
  const inputs = form.elements as unknown as HTMLInputElement[];

  const formData = new FormData();
  formData.append('fullDescription', inputs[3].value);
  formData.append('type', inputs[5].value);
  for (const input of inputs) {
    if (input.type == 'file' && input.files && input.files[0] != undefined) {
      formData.append('productImage', input.files[0]);
      formData.append(input.name, input.files[0].name);
    } else if (input.type == 'text' || input.type == 'number') {
      formData.append(input.name, input.value);
    }
  }

  return postProductWithImages(formData);
};
