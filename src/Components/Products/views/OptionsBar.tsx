import React from 'react';
import { ProductsInterface } from '../Interfaces/productsInterface';
import { ProductsEnums } from '../enums/productsEnums';
import { removeImage } from '../handlers/productsManagments';
import { disablePopup, enablePopup } from '../../../Redux/actions';
import { useDispatch } from 'react-redux';

export const OptionsBar: React.FC<{
  products: ProductsInterface[];
  product: ProductsInterface['_id'];
  disable: () => void;
}> = (props) => {
  const dispatch = useDispatch();

  const disablePanel = (): void => {
    props.disable();
  };

  const handleRemoveImage = (image: string): void => {
    const product = props.products.find((prod) => prod._id == props.product);
    if (product) {
      removeImage(product._id, image)
        .then((data) => {
          if (data) {
            dispatch(enablePopup('Skasowano zdjęcie'));
            return setTimeout(() => {
              dispatch(disablePopup());
            }, 2000);
          } else {
            dispatch(enablePopup('Błąd'));
            return setTimeout(() => {
              dispatch(disablePopup());
            }, 2000);
          }
        })
        .catch((err) => console.log(err));
    }
  };

  const handleRenderImage = (type: string, image: string): JSX.Element => {
    if (image) {
      return (
        <>
          <img src={ProductsEnums.productsLocation + image} className="productImage" alt="productImage" />
          <h2 className="removeImageButton" role="presentation" onClick={(): void => handleRemoveImage(type)}>
            Skasuj zdjęcie
          </h2>
        </>
      );
    }
    return <></>;
  };

  const renderImage = (): JSX.Element => {
    const product = props.products.find((prod) => prod._id == props.product);
    if (product && product.image) {
      return (
        <>
          {handleRenderImage('mainImage', product.image.mainImage)}
          {handleRenderImage('additionalImages', product.image.additionalImages)}
          {handleRenderImage('icon', product.image.icon)}
        </>
      );
    } else {
      return <div className="missingImage">Brak zdjęcia</div>;
    }
  };

  return (
    <div className="imagesRenderer">
      <h2 className="removeImageButton" onClick={(): void => disablePanel()}>
        Wyłącz panel
      </h2>
      {renderImage()}
    </div>
  );
};
