import React from 'react';
import { useDispatch } from 'react-redux';
import { AddNewProductInterface } from '../Interfaces/productsInterface';
import { slideBottom } from '../../Animation/Variables';
import { AnimatePresence, motion } from 'framer-motion';
import { handleSubmit } from '../handlers/productsHandlers';
import { disablePopup, enablePopup } from '../../../Redux/actions';

export const AddNewProduct: React.FC<AddNewProductInterface> = (props) => {
  const dispatch = useDispatch();

  return (
    <AnimatePresence exitBeforeEnter>
      <motion.div className="productsFormContainer" variants={slideBottom} initial="init" animate="visible" exit="exit">
        <form
          className="productsForm"
          onSubmit={async (e): Promise<void> => {
            e.preventDefault();
            try {
              await handleSubmit(e.target as HTMLFormElement).then((data) => {
                if (data) {
                  dispatch(enablePopup('Dodano'));
                  return setTimeout(() => {
                    dispatch(disablePopup());
                  }, 2000);
                } else {
                  dispatch(enablePopup('Błąd'));
                  return setTimeout(() => {
                    dispatch(disablePopup());
                  }, 2000);
                }
              });
            } catch (err) {
              console.log(err);
            }
          }}
        >
          <label htmlFor="name">
            <span>Nazwa</span>
          </label>
          <input type="text" name="name" placeholder="Nazwa" required />
          <p className="formError name" />

          <label htmlFor="price">
            <span>Cena</span>
          </label>
          <input type="number" name="price" placeholder="Cena" required />
          <p className="formError price" />

          <label htmlFor="shortDescription">
            <span>Krótki opis</span>
          </label>
          <input type="text" name="shortDescription" placeholder="Krótki opis" min={10} max={100} required />
          <p className="formError shortDescription" />

          <label htmlFor="fullDescription">
            <span>Długi opis</span>
          </label>
          <textarea name="fullDescription" placeholder="Długi opis" cols={40} rows={5} minLength={20} maxLength={300} />
          <p className="formError fullDescription" />

          <label htmlFor="size">
            <span>Wymiary</span>
          </label>
          <input type="text" name="size" placeholder="Wymiary" required />
          <p className="formError size" />

          <label htmlFor="type">
            <span>Typ produktu</span>
            <select name="type" defaultChecked required>
              <option value="posciele" defaultValue="posciele">
                Pościel
              </option>
              <option value="przescieradla">Prześcieradło</option>
              <option value="poduszki">Poduszka</option>
              <option value="koce">Koc</option>
              <option value="koldry">Kołdra</option>
              <option value="reczniki">Ręcznik</option>
            </select>
          </label>

          <label htmlFor="color">
            <span>Kolor</span>
          </label>
          <input type="text" name="color" placeholder="Kolor" required />
          <p className="formError color" />

          <label htmlFor="amount">
            <span>Ilość</span>
          </label>
          <input type="number" name="amount" placeholder="Ilość" required />
          <p className="formError amount" />

          <label htmlFor="mainImage">Zdjęcie główne</label>
          <input type="file" name="mainImage" />
          <p className="formError mainImage" />

          <label htmlFor="additionalImages">Zdjęcie dodatkowe</label>
          <input type="file" name="additionalImages" />
          <p className="formError additionalImages" />

          <label htmlFor="icon">Ikonka</label>
          <input type="file" name="icon" />
          <p className="formError icon" />

          <button type="submit" onClick={(): void => props.disableAdd()}>
            Zatwierdź
          </button>
          <h2 className="exit" role="presentation" onClick={(): void => props.disableAdd()}>
            X
          </h2>
        </form>
      </motion.div>
    </AnimatePresence>
  );
};
