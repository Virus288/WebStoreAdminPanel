import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addProducts, disablePopup, enablePopup } from '../../../Redux/actions';
import { getAllProducts, removeProduct } from '../handlers/productsManagments';
import { ProductsInterface, ProductsInterfaceWithoutImage } from '../Interfaces/productsInterface';
import { RootState } from '../../../index';
import { EditProduct } from './EditProduct';
import { AddNewProduct } from './AddNewProduct';
import { OptionsBar } from './OptionsBar';

export const Products: React.FC = () => {
  const dispatch = useDispatch();

  const allProducts = useSelector((state: RootState) => state.products.products);

  const [products, setProducts] = useState<ProductsInterface[]>([]);
  const [filteredProducts, setFilteredProducts] = useState<ProductsInterface[]>([]);
  const [enabled, setEnabled] = useState(Array);
  const [edit, setEdit] = useState<boolean>(false);
  const [add, setAdd] = useState<boolean>(false);
  const [selected, setSelected] = useState<ProductsInterface>();
  const [options, setOptions] = useState<boolean>(false);
  const [view, setView] = useState<ProductsInterface['_id']>('none');

  useEffect(() => {
    if (allProducts.length > 0) {
      setProducts(allProducts);
      setFilteredProducts(products);
    } else {
      getAllProducts().then((products) => {
        dispatch(addProducts(products as ProductsInterface[]));
        if (typeof products == 'object') {
          setFilteredProducts(products as ProductsInterface[]);
          setProducts(products as ProductsInterface[]);
        }
      });
    }
  }, [products]);

  const handleRemoveProduct = (product: ProductsInterface): void => {
    removeProduct(product.type, product._id).then((data) => {
      if (data.success) {
        dispatch(addProducts(products.filter((prod) => prod._id != product._id)));
        setFilteredProducts(products.filter((prod) => prod._id != product._id));
        setProducts(products.filter((prod) => prod._id != product._id));
        dispatch(enablePopup('Skasowano'));
        return setTimeout(() => {
          dispatch(disablePopup());
        }, 2000);
      } else {
        dispatch(enablePopup('Błąd'));
        return setTimeout(() => {
          dispatch(disablePopup());
        }, 2000);
      }
    });
  };

  const toogleTR = (itemID: string): void => {
    const toggleALL = document.querySelector('.tableInputAll') as HTMLInputElement;

    if (enabled.includes(itemID)) {
      let innerData = enabled;
      innerData = innerData.filter((item) => {
        return item !== itemID;
      });
      setEnabled(innerData);
      document.getElementById(`${itemID}`)?.classList.toggle('productTR-active');
      document.getElementById(`${itemID}`)?.classList.toggle('productTR-inactive');

      toggleALL.checked = false;
    } else {
      const innerData = enabled;
      innerData.push(itemID);
      setEnabled(innerData);
      document.getElementById(`${itemID}`)?.classList.toggle('productTR-inactive');
      document.getElementById(`${itemID}`)?.classList.toggle('productTR-active');

      const allInactive = document.querySelectorAll('.productTR-inactive');
      if (allInactive.length === 0) {
        toggleALL.checked = true;
      }
    }
  };

  const sort = (type: keyof ProductsInterfaceWithoutImage): void => {
    const newProduct = filteredProducts.sort((a, b) => {
      if (a[type] < b[type]) return -1;
      if (a[type] > b[type]) return 1;
      return 0;
    });
    setFilteredProducts(newProduct);
  };

  const preSort = (): void => {
    const filterValue = document.querySelector('.filter') as HTMLInputElement;
    sort(filterValue.value as keyof ProductsInterfaceWithoutImage);
  };

  const toggleAll = (item: HTMLInputElement): void => {
    let localData = enabled;
    const allInactive = document.querySelectorAll('.productTR-inactive');
    const allActive = document.querySelectorAll('.productTR-active');

    if (item.checked) {
      allInactive.forEach((element) => {
        const checkbox = element.children[0].children[0] as HTMLInputElement;

        element.classList.toggle('productTR-inactive');
        element.classList.toggle('productTR-active');
        checkbox.checked = true;

        localData.push(element.id);
      });
    } else {
      allActive.forEach((element) => {
        const checkbox = element.children[0].children[0] as HTMLInputElement;

        element.classList.toggle('productTR-inactive');
        element.classList.toggle('productTR-active');
        checkbox.checked = false;
      });
      localData = [];
    }

    setEnabled(localData);
  };

  const noEmployees = (): JSX.Element => {
    return <span>Wygląda na to, iż nie dodałeś żadnych produktów.</span>;
  };

  const showOptions = (target: HTMLElement): void => {
    setOptions(true);
    if (target.parentElement && target.parentElement.parentElement) {
      setView(target.parentElement?.parentElement?.id);
    }
  };

  const renderProducts = (): JSX.Element[] => {
    return filteredProducts.map((product) => {
      return (
        <tr id={product._id} key={product._id} className="productTR-inactive">
          <td className="tableInputTd">
            <input
              type="checkbox"
              className="tableInput"
              onChange={(e): void => {
                const target = e.target as Element;
                toogleTR(target.parentElement?.parentElement?.id ?? '');
              }}
            />
          </td>
          <td>{product.name}</td>
          <td>{product.price} zł</td>
          <td>{product.type}</td>
          <td>{product.image != undefined ? Object.keys(product.image).length : 'Brak'}</td>
          <td>{product.ready}</td>
          <td style={{ textAlign: 'right', maxWidth: '50px' }}>
            <button className="productOptionsButton" onClick={(e): void => showOptions(e.target as HTMLElement)}>
              Opcje
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <div className="employeesRender">
      <div className="employeesTop">
        <span>
          <input
            type="text"
            placeholder="Szukaj"
            className="findEmployees"
            onChange={(e): void => {
              if (e.target.value.length === 0) {
                setFilteredProducts(products);
              } else {
                const newProducts: ProductsInterface[] = [];

                for (let x = 0; x < products.length; x++) {
                  if (
                    products[x].name.toLowerCase().includes(e.target.value.toLowerCase()) ||
                    products[x].price.toString().toLowerCase().includes(e.target.value.toLowerCase()) ||
                    products[x].type.toString().toLowerCase().includes(e.target.value.toLowerCase())
                  ) {
                    newProducts.push(products[x]);
                  }
                }
                setFilteredProducts(newProducts);
              }
            }}
          />
          <h4 role="presentation" className="addEmployee" onClick={(): void => setAdd(true)}>
            + Dodaj produkt
          </h4>
        </span>

        <span>
          <span>
            <span>
              <input
                type="checkbox"
                className="tableInputAll"
                style={{
                  height: '30px',
                  marginBottom: '7px',
                  marginRight: '5px',
                }}
                onChange={(e): void => {
                  const target = e.target as HTMLInputElement;
                  toggleAll(target);
                }}
              />
              <h4
                role="presentation"
                onClick={(): void => {
                  const target = document.querySelector('.tableInputAll') as HTMLInputElement;
                  target.checked = !target.checked;
                  toggleAll(target);
                }}
              >
                Wybierz wszystko
              </h4>
            </span>
            <h4
              role="presentation"
              onClick={(): void => {
                const allActive = document.querySelectorAll('.productTR-active');
                if (allActive.length > 1 || allActive.length === 0) {
                  dispatch(enablePopup('Wybierz 1 produkt, który chcesz edytować'));
                  setTimeout(() => {
                    dispatch(disablePopup());
                  }, 3000);
                } else {
                  setSelected(products.find((product) => product._id === allActive[0].id));
                  setEdit(true);
                  allActive.forEach((element) => {
                    const checkbox = element.children[0].children[0] as HTMLInputElement;

                    element.classList.toggle('productTR-inactive');
                    element.classList.toggle('productTR-active');
                    checkbox.checked = false;
                  });
                }
              }}
            >
              Edytuj
            </h4>
            <h4
              role="presentation"
              onClick={(): void => {
                const allActive = document.querySelectorAll('.productTR-active');
                const ids: string[] = [];

                allActive.forEach((item) => {
                  ids.push(item.id);
                });
                ids.forEach((id) => {
                  const product = products.find((product) => product._id == id);
                  if (product) handleRemoveProduct(product);
                });
              }}
            >
              Usuń
            </h4>
          </span>

          <span>
            <h4>Sortuj</h4>
            <select className="filter" name="filter" onChange={(): void => preSort()}>
              <option value="_id" defaultValue="_id">
                Id
              </option>
              <option value="name">Nazwa</option>
              <option value="price">Cena</option>
              <option value="type">Typ</option>
              <option value="ready">Status</option>
            </select>
          </span>
        </span>
      </div>
      <table className="employeeseTable">
        <tbody>
          <tr>
            <td style={{ maxWidth: '20px' }} />
            <td>Nazwa</td>
            <td>Cena</td>
            <td>Typ</td>
            <td>Zdjęcia</td>
            <td>Status:</td>
            <td style={{ textAlign: 'right', maxWidth: '50px' }} />
          </tr>
          {products.length > 0 ? (
            renderProducts()
          ) : (
            <tr>
              <td style={{ display: 'none' }} />
            </tr>
          )}
        </tbody>
      </table>
      {products.length > 0 ? '' : noEmployees()}
      {add ? (
        <AddNewProduct
          setFiltered={setFilteredProducts}
          setProducts={setProducts}
          disableAdd={(): void => setAdd(false)}
        />
      ) : (
        ''
      )}
      {edit ? (
        selected != undefined ? (
          <EditProduct product={selected} disableEdit={(): void => setEdit(false)} />
        ) : (
          dispatch(enablePopup('Nie wybrano produktu'))
        )
      ) : (
        ''
      )}
      {options ? (
        products ? (
          view ? (
            <OptionsBar products={products} product={view} disable={(): void => setOptions(false)} />
          ) : (
            'Nie wybrano produktu'
          )
        ) : (
          'Nie wybrano produktu'
        )
      ) : (
        ''
      )}
    </div>
  );
};
