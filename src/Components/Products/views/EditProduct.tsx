import React, { useEffect } from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import { useDispatch } from 'react-redux';
import { slideBottom } from '../../Animation/Variables';
import { disablePopup, enablePopup } from '../../../Redux/actions';
import { EditProductInterfaceNew } from '../Interfaces/productsInterface';
import { editProductWithImages } from '../handlers/productsManagments';

export const EditProduct: React.FC<EditProductInterfaceNew> = (props) => {
  const { product } = props;
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      fillData();
    }, 500);
  }, []);

  const fillData = (): void => {
    document.querySelectorAll('input').forEach((input) => {
      switch (input.name) {
        case 'name':
          return (input.value = product.name);
        case 'price':
          return (input.value = product.price.toString());
        case 'type':
          return (input.value = product.type);
        case 'amount':
          return (input.value = product.amount.toString());
        case 'size':
          return (input.value = product.size);
        case 'color':
          return (input.value = product.color);
        case 'fullDescription':
          return (input.value = product.fullDescription);
        case 'shortDescription':
          return (input.value = product.shortDescription);
        default:
          return null;
      }
    });
  };

  const handleSubmit = async (form: HTMLFormElement): Promise<void> => {
    const inputs = form.elements as unknown as HTMLInputElement[];

    const formData = new FormData();
    formData.append('_id', product._id);
    for (const input of inputs) {
      if (input.type == 'file' && input.files && input.files[0] != undefined) {
        formData.append('productImage', input.files[0]);
        formData.append(input.name, input.files[0].name);
      } else if (input.type == 'text') {
        formData.append(input.name, input.value);
      }
    }

    editProductWithImages(product.type, product._id, formData)
      .then((data) => {
        if (data) {
          dispatch(enablePopup('Zaktualizowano produkt'));
          return setTimeout(() => {
            dispatch(disablePopup());
          }, 2000);
        } else {
          dispatch(enablePopup('Błąd'));
          return setTimeout(() => {
            dispatch(disablePopup());
          }, 2000);
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <AnimatePresence exitBeforeEnter>
      <motion.div className="productsFormContainer" variants={slideBottom} initial="init" animate="visible" exit="exit">
        <form
          className="productsForm"
          onSubmit={async (e): Promise<void> => {
            e.preventDefault();
            await handleSubmit(e.target as HTMLFormElement);
          }}
        >
          <label htmlFor="name">
            <span>Nazwa</span>
          </label>
          <input type="text" name="name" placeholder="Nazwa" required />
          <p className="formError name" />

          <label htmlFor="price">
            <span>Cena</span>
          </label>
          <input type="number" name="price" placeholder="Cena" required />
          <p className="formError price" />

          <label htmlFor="shortDescription">
            <span>Krótki opis</span>
          </label>
          <input type="text" name="shortDescription" placeholder="Krótki opis" min={10} max={100} required />
          <p className="formError shortDescription" />

          <label htmlFor="fullDescription">
            <span>Długi opis</span>
          </label>
          <textarea name="fullDescription" placeholder="Długi opis" cols={40} rows={5} minLength={20} maxLength={300} />
          <p className="formError fullDescription" />

          <label htmlFor="size">
            <span>Wymiary</span>
          </label>
          <input type="text" name="size" placeholder="Wymiary" required />
          <p className="formError size" />

          <label htmlFor="type">
            <span>Typ produktu</span>
            <select name="type" defaultChecked required>
              <option value="posciele" defaultValue="posciele">
                Pościel
              </option>
              <option value="przescieradla">Prześcieradło</option>
              <option value="poduszki">Poduszka</option>
              <option value="koce">Koc</option>
              <option value="koldry">Kołdra</option>
              <option value="reczniki">Ręcznik</option>
            </select>
          </label>

          <label htmlFor="color">
            <span>Kolor</span>
          </label>
          <input type="text" name="color" placeholder="Kolor" required />
          <p className="formError color" />

          <label htmlFor="amount">
            <span>Ilość</span>
          </label>
          <input type="number" name="amount" placeholder="Ilość" required />
          <p className="formError amount" />

          <label htmlFor="mainImage">Zdjęcie główne</label>
          <input type="file" name="mainImage" />
          <p className="formError mainImage" />

          <label htmlFor="additionalImages">Zdjęcie dodatkowe</label>
          <input type="file" name="additionalImages" />
          <p className="formError additionalImages" />

          <label htmlFor="icon">Ikonka</label>
          <input type="file" name="icon" />
          <p className="formError icon" />

          <button type="submit">Zatwierdź</button>
          <h2 className="exit" role="presentation" onClick={(): void => props.disableEdit()}>
            X
          </h2>
        </form>
      </motion.div>
    </AnimatePresence>
  );
};
