// External function
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../index';

// Internal modules
import { Login } from '../Account/Views/Login';

export const HomePanel: React.FC = () => {
  const username = useSelector((state: RootState) => state.isLogged.isLogged);

  const renderComponents = (): JSX.Element => {
    switch (username) {
      case true:
        return <h2>Zalogowany</h2>;
      case false:
        return <Login />;
      default:
        return <Login />;
    }
  };

  return renderComponents();
};
