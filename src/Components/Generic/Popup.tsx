import React from 'react';
import { RootState } from '../../index';
import { useSelector } from 'react-redux';
import { motion, AnimatePresence } from 'framer-motion';

// Animation data
import { slideBottom } from '../Animation/Variables';

export const Popup: React.FC = () => {
  const popupStatus = useSelector((state: RootState) => state.popup.toggle);
  const popupData = useSelector((state: RootState) => state.popup.text);

  return (
    <AnimatePresence exitBeforeEnter>
      {popupStatus ? (
        <motion.div className="popup" variants={slideBottom} initial="init" animate="visible" exit="exit">
          <h2 className="popupInner">{popupData}</h2>
        </motion.div>
      ) : (
        ' '
      )}
    </AnimatePresence>
  );
};
