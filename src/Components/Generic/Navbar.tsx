import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../index';
import { slideRight } from '../Animation/Variables';
import { logOut } from '../../Redux/actions';
import { logUserOut } from '../Account/Handlers/loginHandlers';

export const Navbar: React.FC = () => {
  const isLogged = useSelector((state: RootState) => state.isLogged.isLogged) == true;

  const dispatch = useDispatch();

  useEffect(() => {
    document.querySelectorAll('.navbarLink').forEach((link) => {
      link.addEventListener('click', (e) => activateLink(e.target as HTMLElement));
    });
    document.querySelectorAll('.navbarElement').forEach((element) => {
      element.addEventListener('click', () => disableActiveLink());
    });
  });

  const activateLink = (navElement: HTMLElement): void => {
    const activeLink = document.querySelector('.navbar-icon-active');
    if (activeLink) {
      activeLink.classList.toggle('navbar-icon-active');
    }
    navElement.classList.toggle('navbar-icon-active');
  };

  const disableActiveLink = (): void => {
    const activeLink = document.querySelector('.navbar-icon-active');
    if (activeLink) {
      activeLink.classList.toggle('navbar-icon-active');
    }
  };

  const handleLogoutUser = (): void => {
    if (isLogged) {
      logUserOut().then(() => dispatch(logOut()));
    }
  };

  return (
    <>
      <motion.div className="navbar" variants={slideRight} initial="init" animate="visible">
        <motion.span
          className="logo"
          initial={{
            x: -250,
          }}
          animate={{
            x: 0,
          }}
          transition={{
            delay: 2,
            type: 'spring',
            stiffness: 50,
          }}
        >
          <Link to="/" style={{ textDecoration: 'none' }}>
            <h5 className="navbarElement">Capri</h5>
          </Link>
        </motion.span>
        <span className="links">
          <Link to="/produkty" style={{ textDecoration: 'none' }}>
            <i className="icon-basket navbarLink"> </i>
          </Link>
          <Link to="/uzytkownicy" style={{ textDecoration: 'none' }}>
            <i className="icon-adult navbarLink"> </i>
          </Link>
          <Link to="/zamowienia" style={{ textDecoration: 'none' }}>
            <i className="icon-doc navbarLink"> </i>
          </Link>
          <Link to="/wiadomosci" style={{ textDecoration: 'none' }}>
            <i className="icon-mail navbarLink"> </i>
          </Link>
        </span>
        <span className="navbarLogoutIcon">
          <i className="icon-logout navbarElement" onClick={(): void => handleLogoutUser()} />
        </span>
      </motion.div>
    </>
  );
};
