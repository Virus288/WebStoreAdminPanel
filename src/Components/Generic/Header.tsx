import React from 'react';
import { motion } from 'framer-motion';

// Animation
import { slideRight } from '../Animation/Variables';

export interface headerinterface {
  category: string;
}

export const CategoryHeader: React.FC<headerinterface> = (data) => {
  return (
    <motion.h2 className="categoryLogo" variants={slideRight} initial="init" animate="visible" exit="exit">
      {data.category}
    </motion.h2>
  );
};
