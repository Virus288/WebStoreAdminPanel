import React from 'react';
import { motion } from 'framer-motion';

// Animation
import { opacity } from '../../Animation/Variables';

export const Messages: React.FC = () => {
  return (
    <motion.div className="productsView" variants={opacity} initial="init" animate="visible" exit="exit">
      Wiadomości
    </motion.div>
  );
};
