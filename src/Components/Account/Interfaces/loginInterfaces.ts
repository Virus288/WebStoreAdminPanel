export interface LoginData {
  email: string;
  password: string;
}

export interface LoginSuccess {
  role: string;
  username: string;
  error: string;
  errors: unknown;
  verified: boolean;
}
