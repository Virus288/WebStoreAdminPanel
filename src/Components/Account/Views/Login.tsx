import React from 'react';
import { motion } from 'framer-motion';
import { useDispatch } from 'react-redux';
import * as actions from '../../../Redux/actions';
import { slideBottom } from '../../Animation/Variables';
import { errorCodesHandler } from '../../../Errors/errorCodesHandler';
import { logIn } from '../Handlers/loginHandlers';

export const Login: React.FC = () => {
  const dispatch = useDispatch();

  const submitForm = (e: React.FormEvent): Promise<void> => {
    e.preventDefault();
    const target = e.target as HTMLFormElement;
    const email = target.email.value;
    const password = target.password.value;

    return logIn({ email, password })
      .then((data) => {
        if (data.success) {
          dispatch(actions.logIn());
        } else {
          dispatch(actions.enablePopup(data.message));
          setTimeout(() => {
            dispatch(actions.disablePopup());
          }, 3500);
        }
      })
      .catch((err) => {
        errorCodesHandler(err);
      });
  };

  return (
    <motion.div className="accountForm" variants={slideBottom} initial="init" animate="visible" exit="exit">
      <form
        onSubmit={async (e): Promise<void> => {
          await submitForm(e);
        }}
      >
        <h2 className="loginHelperHeader">Logowanie</h2>

        <div className="input">
          <label htmlFor="email">Email</label>
          <input type="email" name="email" placeholder="Email" required />
        </div>
        <div className="email error" />

        <div className="input">
          <label htmlFor="email">Hasło</label>
          <input type="password" name="password" placeholder="Hasło" required />
        </div>
        <div className="password error" />

        <button>Zaloguj</button>
      </form>
    </motion.div>
  );
};
