import { errorCodesHandler } from '../../../Errors/errorCodesHandler';
import { LoginData, LoginSuccess } from '../Interfaces/loginInterfaces';
import { LoginSuccessInterface } from '../../Products/Interfaces/productsInterface';

export const checkIfLogged = async (): Promise<LoginSuccessInterface> => {
  try {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/login`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
    });
    const postData = await res.json();
    if (!res.ok) {
      return errorCodesHandler({ errCode: res.status, error: postData.error });
    } else if (res.status === 200) {
      return { success: true, message: 'success' };
    }
  } catch (e) {
    console.log(e);
  }
  return { success: false, message: 'Błąd' };
};

export const logUserOut = async (): Promise<void> => {
  fetch(`${process.env.REACT_APP_BACKEND}/logout`, {
    method: 'GET',
    credentials: 'include',
  }).then((res) => {
    return res.json();
  });
};

export const logIn = async (data: LoginData): Promise<LoginSuccessInterface> => {
  try {
    const res = await fetch(`${process.env.REACT_APP_BACKEND}/login`, {
      method: 'POST',
      body: JSON.stringify({ email: data.email, password: data.password }),
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
    });
    const postData: LoginSuccess = await res.json();
    if (!res.ok) {
      return errorCodesHandler({ errCode: res.status, error: postData.error });
    } else if (res.status === 200) {
      return { success: true, message: 'success' };
    }
  } catch (e) {
    console.log(e);
  }
  return { success: false, message: 'Błąd' };
};
